export default {
	goodsList: [{
			id: 0,
			thumbUrl: '//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1532604005.80875371.jpg?thumb=1&w=250&h=250',
			name: '小米路由器4C 白色',
			desc: '300M单频，高增益4天线',
			currentPrice: 59,
			originalPrice: 99
		},
		{
			id: 0,
			thumbUrl: '//cdn.cnbj0.fds.api.mi-img.com/b2c-miapp-a1/7e206c06-260d-592a-3253-84c3f597a7bf.jpg?thumb=1&w=250&h=250',
			name: '米家扫地机器人',
			desc: '智商高，扫得干净扫得快',
			currentPrice: 1295,
			originalPrice: 1699
		},
		{
			id: 0,
			thumbUrl: '//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1571038766.41824452.jpg?thumb=1&w=250&h=250',
			name: 'MIJOY 抽纸青春版 24包/箱',
			desc: '精选原生竹浆，健康环保',
			currentPrice: 27.9,
			originalPrice: 32.9
		},
		{
			id: 0,
			thumbUrl: '//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1560158562.71434641.jpg?thumb=1&w=250&h=250',
			name: '米家智能门锁 推拉式 通用版',
			desc: '一步推拉，高端智能门锁',
			currentPrice: 1599,
			originalPrice: 1699
		},
		{
			id: 0,
			thumbUrl: '//cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/04d522108145d58ee53b802f28bb6357.jpg?thumb=1&w=250&h=250',
			name: '米家LED吸顶灯 白色',
			desc: '用光线，还原理想生活',
			currentPrice: 349,
			originalPrice: 399
		},
		{
			id: 0,
			thumbUrl: '//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1563953638.84752422.jpg?thumb=1&w=250&h=250',
			name: '小米米家智能门锁 青春版 左开门',
			desc: '隐形指纹识别设计 简单一步快进家门',
			currentPrice: 995,
			originalPrice: 999
		},
		{
			id: 0,
			thumbUrl: '//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1573031970.47458207.jpg?thumb=1&w=250&h=250',
			name: '小米随身蓝牙音箱 无线立体声2个装 黑色',
			desc: '随身随地享受震撼立体声',
			currentPrice: 99,
			originalPrice: 129
		},
		{
			id: 0,
			thumbUrl: '//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1571223747.46563922.jpg?thumb=1&w=250&h=250',
			name: '米家扫拖机器人1C 白色',
			desc: '能扫能拖，地面清洁交给我',
			currentPrice: 1249,
			originalPrice: 1299
		},
		{
			id: 0,
			thumbUrl: 'https://cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1565058524.25058398.jpg?thumb=1&w=250&h=250',
			name: '米家飞利浦读写台灯 白色',
			desc: '双重防眩光，专为读写而生',
			currentPrice: 499,
			originalPrice: 569
		},
		{
			id: 0,
			thumbUrl: 'https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/93ef574280f48c407b6e7643c2b2636e.jpg?thumb=1&w=250&h=250&f=webp&q=90',
			name: '小米小爱音箱HD 浅灰色',
			desc: '震撼嘹亮 音智同享',
			currentPrice: 549,
			originalPrice: 599
		}
	],
	colors: ['#ffac13', '#83c44e', '#2196f3', '#e53935', '#00c0a5']
}
